package ru.pisarev.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.model.SessionGraph;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionGraph> {

    List<SessionGraph> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final SessionGraph session);

}
